# Description
Vulkan tutorial from the https://vulkan-tutorial.com/ .
# How to use
1. Set the VULKAN_SDK_HOME enviromental variable to the LunarG SDK home directory - e.g. ~/Programming/C++/VULKAN/VulkanSDK/1.0.61.1/x86_64.
2. Set enviromental variables for the executable 
    * LD_LIBRARY_PATH to $(VULKAN_SDK_PATH)/lib 
    * VK_LAYER_PATH to $(VULKAN_SDK_PATH)/etc/explicit_layer.d .