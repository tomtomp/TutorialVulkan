/**
 * @file Util.h
 * @author Tomas Polasek
 * @brief Miscellaneous utilities.
 */

#ifndef VULKANTUTORIAL_UTIL_H
#define VULKANTUTORIAL_UTIL_H

#include "Types.h"

/// Container for utility functions.
namespace Util
{
    /// Print available Vulkan extensions.
    void uvkPrintExtensions();

    /// Callback function for the Vulkan debug reporting.
    VKAPI_ATTR VkBool32 VKAPI_CALL uvkDebugCallback(
        VkDebugReportFlagsEXT flags,
        VkDebugReportObjectTypeEXT objectType,
        uint64_t object,
        size_t location,
        int32_t messageCode,
        const char* pLayerPrefix,
        const char* pMessage,
        void* pUserData);

    /// Create a VkDebugReportCallbackEXT object.
    VkResult createDebugReportCallbackEXT(
        VkInstance instance,
        const VkDebugReportCallbackCreateInfoEXT *createInfo,
        const VkAllocationCallbacks *allocator,
        VkDebugReportCallbackEXT *callback);

    /// Destroy a VkDebugReportCallbackEXT object.
    void destroyDebugReportCallbackEXT(
        VkInstance instance,
        VkDebugReportCallbackEXT callback,
        const VkAllocationCallbacks *allocator);

    /**
     * Load given file with binary content.
     * @param filename Path to the file.
     * @return Returns binary data read from the file.
     */
    std::vector<char> loadFileB(const std::string &filename);
} // namespace Util

#endif //VULKANTUTORIAL_UTIL_H
