/**
 * @file SimpleVertex.h
 * @author Tomas Polasek
 * @brief Simple vertex data structure capable o being transfered to the GPU.
 */

#ifndef VULKANTUTORIAL_SIMPLEVERTEX_H
#define VULKANTUTORIAL_SIMPLEVERTEX_H

#include "Types.h"

struct SimpleVertex
{
    glm::vec2 pos;
    glm::vec3 col;
    glm::vec2 texCoord;

    // TODO - Ugly.
    static VkVertexInputBindingDescription getBindingDescription()
    {
        VkVertexInputBindingDescription bd{};
        // Buffer binding.
        bd.binding = 0;
        // Number of bytes per vertex input.
        bd.stride = sizeof(SimpleVertex);
        // One vertex per vertex index.
        bd.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
        return bd;
    }

    // TODO - Ugly.
    static std::array<VkVertexInputAttributeDescription, 3> getAttributeDescriptions()
    {
        VkVertexInputAttributeDescription posAD{};
        // Buffer binding.
        posAD.binding = 0;
        // Attribute location.
        posAD.location = 0;
        // Format of the position element, 2 signed floats.
        posAD.format = VK_FORMAT_R32G32_SFLOAT;
        // Offset of the position in the vertex data structure
        posAD.offset = offsetof(SimpleVertex, pos);

        VkVertexInputAttributeDescription colAD{};
        // Buffer binding.
        colAD.binding = 0;
        // Attribute location.
        colAD.location = 1;
        // Format of the color element, 3 signed floats.
        colAD.format = VK_FORMAT_R32G32B32_SFLOAT;
        // Offset of the color in the vertex data structure
        colAD.offset = offsetof(SimpleVertex, col);

        VkVertexInputAttributeDescription texAD{};
        // Buffer binding.
        texAD.binding = 0;
        // Attribute location.
        texAD.location = 2;
        // Format of the color element, 2 signed floats.
        texAD.format = VK_FORMAT_R32G32_SFLOAT;
        // Offset of the texture coordinates in the vertex data structure
        texAD.offset = offsetof(SimpleVertex, texCoord);

        return { posAD, colAD, texAD };
    }
};

namespace ExampleData
{
    static const std::vector<SimpleVertex> vertices = {
        {{ -1.0f, -1.0f }, { 1.0f, 0.0f, 0.0f }, { 0.0f, 0.0f }},
        {{  1.0f, -1.0f }, { 0.0f, 1.0f, 0.0f }, { 1.0f, 0.0f }},
        {{  1.0f,  1.0f }, { 0.0f, 0.0f, 1.0f }, { 1.0f, 1.0f }},
        {{ -1.0f,  1.0f }, { 1.0f, 1.0f, 1.0f }, { 0.0f, 1.0f }}
    };

    static const std::vector<uint16_t> indices = {
            0, 1, 2,
            2, 3, 0
    };
}

#endif //VULKANTUTORIAL_SIMPLEVERTEX_H
