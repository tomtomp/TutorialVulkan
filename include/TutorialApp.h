/**
 * @file TutorialApp.cpp
 * @author Tomas Polasek
 * @brief Class representing the main application.
 */

#ifndef VULKANTUTORIAL_TUTORIALAPP_H
#define VULKANTUTORIAL_TUTORIALAPP_H

#include "Types.h"
#include "Util.h"
#include "Timer.h"
#include "SimpleVertex.h"

/// Main application class.
class TutorialApp
{
public:
    TutorialApp() = default;
    TutorialApp(const TutorialApp &other) = delete;
    TutorialApp(TutorialApp &&other) = delete;
    TutorialApp &operator=(const TutorialApp &rhs) = delete;
    TutorialApp &operator=(TutorialApp &&rhs) = delete;

    /// Main application loop.
    void run();
private:
    /**
     * Structure used for checking available queue families.
     */
    struct QueueFamilyIndices
    { // TODO - Rework needed!
    public:
        /// Index for families which were not found.
        static constexpr int32_t NOT_FOUND{-1};
        /// Index of the graphics family.
        int32_t graphicsFamily{NOT_FOUND};
        /// Index of the presentation family.
        int32_t presentFamily{NOT_FOUND};

        /// Returns true, if all of the requirements were met.
        bool isComplete() {
            return graphicsFamily != NOT_FOUND &&
                   presentFamily != NOT_FOUND;
        }
    private:
    protected:
    };

    /**
     * Structure containing information about swap chain
     * support.
     */
    struct SwapChainSupportDetails
    { // TODO - Rework needed!
        VkSurfaceCapabilitiesKHR capabilities;
        std::vector<VkSurfaceFormatKHR> formats;
        std::vector<VkPresentModeKHR> presentModes;
    };

    /**
     * Layout of the uniform buffer, passed to the
     * shaders.
     */
    struct UniformBufferObject
    {
        glm::mat4 model;
        glm::mat4 view;
        glm::mat4 proj;
    };

    /// Width of the main window in pixels.
    static constexpr uint32_t WINDOW_WIDTH{800};
    /// Height of the main window in pixels.
    static constexpr uint32_t WINDOW_HEIGHT{600};
    /// Title of the main window.
    static constexpr char const *WINDOW_TITLE{"Hello Vulkan!"};
    /// List of validation layers.
    static const std::vector<const char*> VALIDATION_LAYERS;
#ifdef NDEBUG
    /// Are the validations layers enabled?
    static constexpr bool VALIDATION_LAYERS_ENABLE{false};
#else
    /// Are the validations layers enabled?
    static constexpr bool VALIDATION_LAYERS_ENABLE{true};
#endif
    /// List of required device extenstions.
    static const std::vector<const char*> DEVICE_EXTENSIONS;
    /// Main shader name prefix.
    static constexpr char const *SHADER_NAME{"simpleVB"};
    /// Extension of vertex shader.
    static constexpr char const *SHADER_VERT_EXT{".vert"};
    /// Extension of fragment shader.
    static constexpr char const *SHADER_FRAG_EXT{".frag"};
    /// Extension of compiled Spir-V code.
    static constexpr char const *SHADER_SPIRV_EXT{".spv"};
    /// Folder containing compiled shaders..
    static constexpr char const *SHADER_FOLDER{"shaders/"};

    /// Print information every one second.
    static constexpr uint64_t MS_PER_INFO{1000};
    /// Target number of updates per second.
    static constexpr double TARGET_UPS{60.0};
    /// Once in how many milliseconds should state update.
    static constexpr double MS_PER_UPDATE{Timer::MS_IN_S / TARGET_UPS};
    /// Target number of frames per second.
    static constexpr double TARGET_FPS{60.0};
    /// Once in how many milliseconds should a frame be presented.
    static constexpr double MS_PER_FRAME{Timer::MS_IN_S / TARGET_FPS};

    /// Callback for resizing of the main window.
    static void windowResizeCb(GLFWwindow *window, int width, int height);

    /// Initialize the window using GLFW.
    void initWindow();
    /// Initialize Vulkan API.
    void initVulkan();
    /// Main loop of the application.
    void mainLoop();
    /// Cleanup of resources.
    void cleanup();

    /// Initialize the main Vulkan instance.
    void createInstance();
    /// Configuration for validation layer callback.
    void setupDebugCallback();
    /// Create window surface.
    void createSurface();
    /// Pick a suitable physical device.
    void pickPhysicalDevice();
    /// Create a logical device with required queues.
    void createLogicalDevice();
    /// Create the swap chain for main window.
    void createSwapChain();
    /// Create views for images in the main swap chain.
    void createSwapChainViews();
    /// Create image and view for the MSAA.
    void createMSAAImageView();
    /// Create image and view for the depth buffer.
    void createDepthImageView();
    /// Specify rendering process.
    void createRenderPass();
    /// Create layout information about used descriptor sets.
    void createDescriptorSetLayout();
    /// Create the graphics pipeline used in rendering.
    void createGraphicsPipeline();
    /// Create required framebuffers for current swap chain.
    void createFramebuffers();
    /// Create the main graphics queue command pool.
    void createCommandPool();
    /// Create testing texture from an image.
    void createTextureImage();
    /// Create sampler for the texture image.
    void createTextureSampler();
    /// Create image view for the texture.
    void createTextureImageView();
    /// Create buffer for vertices.
    void createVertexBuffer();
    /// Create buffer for indices.
    void createIndexBuffer();
    /// Create buffer for uniforms.
    void createUniformBuffer();
    /// Create pool for creating descriptors.
    void createDescriptorPool();
    /// Create descriptor set for the uniforms.
    void createDescriptorSet();
    /// Create command buffer for each of the framebuffers.
    void createCommandBuffers();
    /// Create semaphores for synchronizing commands in queues.
    void createSemaphores();

    /// Cleanup all of the swap chain Vulkan objects.
    void cleanupSwapChain();
    /// Recreate swap chain in case of window changing.
    void recreateSwapChain();

    /// Run the application logic.
    void updateState();

    /// Update the uniforms on the GPU side.
    void updateUBO();

    /// Draw a single frame using the main pipeline.
    void submitDrawCommands();

    /**
     * Check if all of the validation layers are available in
     * the current instance.
     * @return Returns true, if all of the validations layers
     *   are present.
     */
    bool checkValidationLayers();

    /**
     * Check given physical device, if it supports all required
     * extensions.
     * @param device Device being investigated.
     * @return Returns true, if the device is suitable.
     */
    bool isDeviceSuitable(VkPhysicalDevice device);

    /**
     * Check if given device supports all required extensions.
     * @param device Device being investigated.
     * @return Returns true, if the device supports all required
     *   extensions.
     */
    bool checkDeviceExtensionSupport(VkPhysicalDevice device);

    /**
     * Get vector of all required extensions by name.
     * @return Vector containing all required extensions by name.
     */
    std::vector<const char*> getRequiredExtensions();

    /**
     * Find queue families which we are interested in.
     * @param device Device to search in.
     * @return Returns filled queue families structure.
     */
    QueueFamilyIndices findQueueFamilies(VkPhysicalDevice device);

    /**
     * Find details about swap chain on given device.
     * @param device Physical device being investigated.
     * @return Returns filled structure containing details
     *   about capabilities of swap chain on given device.
     */
    SwapChainSupportDetails querySwapChainDetails(VkPhysicalDevice device);

    /**
     * Choose preferred surface format.
     * @param availableFormats List of available formats.
     * @return Returns chosen surface format.
     */
    VkSurfaceFormatKHR chooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR> &availableFormats);

    /**
     * Choose preferred presentation mode.
     * @param presentModes List of available presentation modes.
     * @return Returns chosen presentation mode.
     */
    VkPresentModeKHR chooseSwapPresentMode(const std::vector<VkPresentModeKHR> &presentModes);

    /**
     * Choose preferred resolution.
     * @param capabilities Capabilities of target surface.
     * @return Returns chosen resolution.
     */
    VkExtent2D chooseSwapExtent(const VkSurfaceCapabilitiesKHR &capabilities);

    /**
     * Create shader module from given Spir-V code.
     * @param shaderSpirV Compiled shader Spir-V code.
     * @return Returns shader module for given shader code.
     */
    VkShaderModule createShaderModule(const std::vector<char> &shaderSpirV);

    /**
     * Select memory, which can be used to store given type of data and support
     * requested operations.
     * @param typeFilter Which type of memory do we need.
     * @param properties Which properties should the memory have.
     * @return Returns index of the memory.
     */
    uint32_t findMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties);

    /**
     * Helper function for creating buffers.
     * @param size Size of the buffer in bytes.
     * @param usage How will the buffer be used.
     * @param properties Required properties of target memory.
     * @param buffer Output for the created buffer.
     * @param memory Output for the memory bound to the buffer.
     */
    void createBuffer(VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties,
                      VkBuffer &buffer, VkDeviceMemory &memory);

    /**
     * Copy content from source buffer to the destination buffer.
     * @param srcBuffer The source buffer.
     * @param dstBuffer The destination buffer.
     * @param size Size of the copied data.
     */
    void copyBuffer(VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size);

    /**
     * Copy content of the buffer to given image.
     * @param buffer Buffer to copy from.
     * @param image Image to copy to.
     * @param width Width of the image.
     * @param height Height of the image.
     */
    void copyBufferToImage(VkBuffer buffer, VkImage image, uint32_t width, uint32_t height);

    /**
     * Create image with given parameters.
     * @param width Width of the image.
     * @param height Height of the image.
     * @param format Format of the image.
     * @param samples How many samples per texel.
     * @param tiling Tiling settings.
     * @param usageFlags Usage flags.
     * @param memFlags Flags for the memory allocation.
     * @param image Image handle is returned through this reference.
     * @param memory Memory handle is returned through this reference.
     */
    void createImage(uint32_t width, uint32_t height, VkFormat format,
                     VkSampleCountFlagBits samples, VkImageTiling tiling,
                     VkImageUsageFlags usageFlags, VkMemoryPropertyFlags memFlags,
                     VkImage &image, VkDeviceMemory &memory);

    /**
     * Create Image view for given image.
     * @param image Image to create view for.
     * @param format Format of the view.
     * @param aspectFlags Flags for the view.
     * @param imageView Image view handle is returned through this reference.
     */
    void createImageView(const VkImage &image, VkFormat format,
                         VkImageAspectFlags aspectFlags, VkImageView &imageView);

    /**
     * Find supported format for images, based on the list of candidates.
     * @param candidates List of candidates, first satisfactory candidate will
     *   be returned.
     * @param tiling Requested type of tiling.
     * @param features Target usage.
     * @return Returns first supported candidate.
     */
    VkFormat findSupportedFormat(const std::initializer_list<VkFormat>& candidates,
                                 VkImageTiling tiling, VkFormatFeatureFlags features);

    /**
     * Find image format for depth buffer.
     * @return Returns format used for the depth buffer.
     */
    VkFormat findDepthFormat();

    /**
     * Does given image format have a stencil memory?
     * @param format Tested format.
     * @return Returns true, if the format has stencil bits.
     */
    bool hasStencilComponent(VkFormat format);

    /**
     * Create a command buffer for single use.
     * @return Returns the command buffer.
     */
    VkCommandBuffer beginTransientCommands();

    /**
     * Execute commands and destroy command buffer.
     * @param commandBuffer Command buffer to use.
     */
    void endTransientCommands(VkCommandBuffer &commandBuffer);

    // TODO - void setupCommandBuffer() = Accumulator for commands.
    // TODO - void flushSetupCommands(...) = Execute accumulated commands.

    /**
     * Calculate aspect mask based on the format and new layout.
     * @param format Format of the transitioned image.
     * @param newLayout Target layout of the transitioned image.
     * @return Returns correct aspect flags.
     */
    VkImageAspectFlags transitionImageAspects(VkFormat format, VkImageLayout newLayout);

    /**
     * Transition layout of an image from old layout
     * to the new one.
     * @param image Image to change the layout of.
     * @param format Format of the image.
     * @param oldLayout Old layout. Can be set to UNDEFINED if the content
     *   is of no concern.
     * @param newLayout New layout.
     */
    void transitionImageLayout(VkImage image, VkFormat format,
                               VkImageLayout oldLayout, VkImageLayout newLayout);

    /// Main application window.
    GLFWwindow *mWindow;

    // TODO - Create destroy wrappers for Vulkan objects!
    /// Main Vulkan instance.
    VkInstance mInstance;
    /// Debug callback used with validation layers.
    VkDebugReportCallbackEXT mDebugCallback;
    /// Surface of the main window.
    VkSurfaceKHR mSurface{VK_NULL_HANDLE};
    /// Physical device used by the application.
    VkPhysicalDevice mPhysicalDevice{VK_NULL_HANDLE};
    /// Logical device used by the application.
    VkDevice mDevice{VK_NULL_HANDLE};
    /// Graphics queue using the main logical device.
    VkQueue mGraphicsQueue{VK_NULL_HANDLE};
    /// Queue used for window presentation.
    VkQueue mPresentQueue{VK_NULL_HANDLE};
    /// Swap chain for the main window.
    VkSwapchainKHR mSwapChain{VK_NULL_HANDLE};
    /// List of images inside the current swap chain.
    std::vector<VkImage> mSwapChainImages;
    /// Format of the current swap chain.
    VkFormat mSwapChainFormat;
    /// Extent of the current swap chain.
    VkExtent2D mSwapChainExtent;
    /// Image views for the current swap chain.
    std::vector<VkImageView> mSwapChainViews;
    /// Base image for multisampling.
    VkImage mMSAAImage;
    /// Memory for the multisampled image.
    VkDeviceMemory mMSAAImageMemory;
    /// View for the multisampled image.
    VkImageView mMSAAImageView;
    /// Base image for containing depth.
    VkImage mDepthImage;
    /// Memory for depth buffer.
    VkDeviceMemory mDepthImageMemory;
    /// View for the depth buffer.
    VkImageView mDepthImageView;
    /// Multisampled depth image.
    VkImage mMSAADepthImage;
    /// Memory for the multisampled depth image.
    VkDeviceMemory mMSAADepthImageMemory;
    /// View for the multisampled depth image.
    VkImageView mMSAADepthImageView;
    /// Main render pass.
    VkRenderPass mRenderPass;
    /// Main viewport.
    VkViewport mViewport;
    /// Descriptor set layout for the main pipeline.
    VkDescriptorSetLayout mDescriptorSetLayout;
    /// Main pipeline layout, uniform mapping.
    VkPipelineLayout mPipelineLayout;
    /// Main graphics pipeline.
    VkPipeline mPipeline;
    /// List of framebuffers for current swap chain.
    std::vector<VkFramebuffer> mSwapChainFramebuffers;
    /// Main graphic queue command pool.
    VkCommandPool mCommandPool;
    /// Main descriptor pool.
    VkDescriptorPool mDescriptorPool;
    /// Uniform descriptor set.
    VkDescriptorSet mDescriptorSet;
    /// List of command buffers, each for specific framebuffer.
    std::vector<VkCommandBuffer> mCommandBuffers;
    /// Semaphore signalizing when an image is ready for rendering.
    VkSemaphore mImageAvailableSem;
    /// Semaphore signalizing when renderin is finished.
    VkSemaphore mRenderFinishedSem;

    /// Buffer containing vertices.
    VkBuffer mVertexBuffer;
    /// Memory on the gpu, containing vertex data.
    VkDeviceMemory mVertexMemory;

    /// Buffer containing indices.
    VkBuffer mIndexBuffer;
    /// Memory on the gpu, containing indice data.
    VkDeviceMemory  mIndexMemory;

    /// Buffer containing uniforms.
    VkBuffer mUniformBuffer;
    /// Memory on the gpu, containing uniform data.
    VkDeviceMemory mUniformMemory;

    /// Uniform variables.
    UniformBufferObject mUniforms;

    /// Host visible staging buffer.
    VkBuffer mStagingBuffer;
    /// Host visible memory for the staging buffer.
    VkDeviceMemory mStagingBufferMemory;
    /// Image for texture sampling.
    VkImage mTextureImage;
    /// Memory for the texture sampler.
    VkDeviceMemory mTextureImageMemory;
    /// Image view for the texture sampler.
    VkImageView mTextureImageView;
    /// Actual sampler for the texture.
    VkSampler mTextureSampler;
};

#endif //VULKANTUTORIAL_TUTORIALAPP_H
