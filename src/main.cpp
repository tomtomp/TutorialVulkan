#include "main.h"

int main()
{
    try {
        TutorialApp app;
        app.run();
    } catch (const std::runtime_error &e) {
        std::cout << "Exception occurred: " << e.what() << std::endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}