/**
 * @file Util.h
 * @author Tomas Polasek
 * @brief Miscellaneous utilities.
 */

#include "Util.h"

namespace Util
{
    void uvkPrintExtensions()
    {
        uint32_t extensionCount{0};
        vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, nullptr);
        std::vector<VkExtensionProperties> supportedExtensions(extensionCount);
        vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, supportedExtensions.data());

        std::cout << "Supported extensions : " << extensionCount << std::endl;
        for (const auto &extension : supportedExtensions)
        {
            std::cout << "\t" << extension.extensionName << " (" << extension.specVersion << ")\n";
        }
        std::cout << "End of extensions list." << std::endl;
    }

    VKAPI_ATTR VkBool32 VKAPI_CALL uvkDebugCallback(
        VkDebugReportFlagsEXT flags,
        VkDebugReportObjectTypeEXT objectType,
        uint64_t object,
        size_t location,
        int32_t messageCode,
        const char* pLayerPrefix,
        const char* pMessage,
        void* pUserData)
    {
        std::cerr << pLayerPrefix << " : " << pMessage << std::endl;

        return VK_FALSE;
    }

    VkResult createDebugReportCallbackEXT(
        VkInstance instance,
        const VkDebugReportCallbackCreateInfoEXT *createInfo,
        const VkAllocationCallbacks *allocator,
        VkDebugReportCallbackEXT *callback)
    {
        auto createFun = reinterpret_cast<PFN_vkCreateDebugReportCallbackEXT>(
            vkGetInstanceProcAddr(instance, "vkCreateDebugReportCallbackEXT")
        );
        if (createFun)
        {
            return createFun(instance, createInfo, allocator, callback);
        }

        return VK_ERROR_EXTENSION_NOT_PRESENT;
    }

    void destroyDebugReportCallbackEXT(
        VkInstance instance,
        VkDebugReportCallbackEXT callback,
        const VkAllocationCallbacks *allocator)
    {
        auto destroyFun = reinterpret_cast<PFN_vkDestroyDebugReportCallbackEXT>(
            vkGetInstanceProcAddr(instance, "vkDestroyDebugReportCallbackEXT")
        );
        if (destroyFun)
        {
            destroyFun(instance, callback, allocator);
        }
    }

    std::vector<char> loadFileB(const std::string &filename)
    {
        // Start at the end of the file and open as binary.
        std::ifstream file(filename, std::ios::ate | std::ios::binary);

        if (!file.is_open())
        {
            throw std::runtime_error(std::string("Unable to open file: ") + filename);
        }

        std::size_t fileSize{static_cast<std::size_t>(file.tellg())};
        std::vector<char> fileContent(fileSize);

        file.seekg(0);
        file.read(fileContent.data(), fileContent.size());

        file.close();

        return fileContent;
    }
} // namespace Util
