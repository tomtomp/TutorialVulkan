/**
 * @file TutorialApp.cpp
 * @author Tomas Polasek
 * @brief Class representing the main application.
 */

#include <set>
#include "TutorialApp.h"

const std::vector<const char*> TutorialApp::VALIDATION_LAYERS{
    "VK_LAYER_LUNARG_standard_validation"
};

const std::vector<const char*> TutorialApp::DEVICE_EXTENSIONS{
    VK_KHR_SWAPCHAIN_EXTENSION_NAME
};

void TutorialApp::run()
{
    initWindow();
    initVulkan();

    mainLoop();

    cleanup();
}

void TutorialApp::windowResizeCb(GLFWwindow *window, int width, int height)
{
    if (width == 0 || height == 0)
    {
        return;
    }

    auto app = reinterpret_cast<TutorialApp*>(glfwGetWindowUserPointer(window));
    app->recreateSwapChain();
}

void TutorialApp::initWindow()
{
    glfwInit();

    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);

    mWindow = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, WINDOW_TITLE, nullptr, nullptr);

    glfwSetWindowUserPointer(mWindow, this);
    glfwSetWindowSizeCallback(mWindow, windowResizeCb);
}

void TutorialApp::initVulkan()
{
    Util::uvkPrintExtensions();

    createInstance();
    setupDebugCallback();
    createSurface();
    pickPhysicalDevice();
    createLogicalDevice();

    createSwapChain();
    createSwapChainViews();
    createMSAAImageView();
    createRenderPass();

    createDescriptorSetLayout();
    createGraphicsPipeline();
    createCommandPool();

    createDepthImageView();
    createFramebuffers();

    createTextureImage();
    createTextureImageView();
    createTextureSampler();
    createVertexBuffer();
    createIndexBuffer();
    createUniformBuffer();

    createDescriptorPool();
    createDescriptorSet();

    createCommandBuffers();
    createSemaphores();
}

void TutorialApp::mainLoop()
{
    // By how much is the state of the lagging behind, in milliseconds.
    double updateLag{0.0};
    double drawLag{0.0};
    // Counters used for debug printing.
    uint64_t frameCounter{0};
    uint64_t updateCounter{0};

    // Timer used for state updating and rendering.
    Timer updateTimer;
    // Timer used for debug printing.
    Timer printTimer;

    while (!glfwWindowShouldClose(mWindow))
    {
        // Calculate by how much the lag should increase.
        if (updateTimer.elapsed<Timer::milliseconds>() > 1.0)
        {
            double lag = updateTimer.elapsedReset<Timer::microseconds>() / Timer::US_IN_MS;
            updateLag += lag;
            drawLag += lag;
        }

        // Test if we should print information.
        uint64_t sincePrint{printTimer.elapsed<Timer::milliseconds>()};
        if (sincePrint > MS_PER_INFO)
        {
            std::cout << "FrameTime[ms] : "
                      << static_cast<double>(sincePrint) / frameCounter
                      << "\nFrames : " << frameCounter
                      << "; Updates : " << updateCounter
                      << std::endl;
            if (drawLag > 2 * MS_PER_FRAME)
            {
                std::cout << "Frame overload: " << drawLag / MS_PER_FRAME << std::endl;
            }

            frameCounter = 0;
            updateCounter = 0;

            printTimer.reset();
        }

        glfwPollEvents();

        while (updateLag >= MS_PER_UPDATE)
        {
            updateState();

            updateCounter++;
            updateLag -= MS_PER_UPDATE;
        }

        if (drawLag >= MS_PER_FRAME)
        {
            // Explicitly synchronize with GPU, to prevent memory leak with validation layers.
            if (VALIDATION_LAYERS_ENABLE)
            {
                vkQueueWaitIdle(mGraphicsQueue);
            }

            submitDrawCommands();

            frameCounter++;
            drawLag -= MS_PER_FRAME;
        }
    }

    std::cout << "Waiting until device finishes all requested operations, before quitting..." << std::endl;
    // Wait until all of the operations are finished before ending.
    vkDeviceWaitIdle(mDevice);
}

void TutorialApp::cleanup()
{
    // TODO - Create wrappers with automatic destruction.

    cleanupSwapChain();
    vkDestroySwapchainKHR(mDevice, mSwapChain, nullptr);

    vkDestroySampler(mDevice, mTextureSampler, nullptr);
    vkDestroyImageView(mDevice, mTextureImageView, nullptr);
    vkDestroyImage(mDevice, mTextureImage, nullptr);
    vkFreeMemory(mDevice, mTextureImageMemory, nullptr);

    vkDestroyDescriptorPool(mDevice, mDescriptorPool, nullptr);

    vkDestroyDescriptorSetLayout(mDevice, mDescriptorSetLayout, nullptr);
    vkDestroyBuffer(mDevice, mUniformBuffer, nullptr);
    vkFreeMemory(mDevice, mUniformMemory, nullptr);

    vkDestroyBuffer(mDevice, mIndexBuffer, nullptr);
    vkFreeMemory(mDevice, mIndexMemory, nullptr);

    vkDestroyBuffer(mDevice, mVertexBuffer, nullptr);
    vkFreeMemory(mDevice, mVertexMemory, nullptr);

    vkDestroySemaphore(mDevice, mRenderFinishedSem, nullptr);
    vkDestroySemaphore(mDevice, mImageAvailableSem, nullptr);

    vkDestroyCommandPool(mDevice, mCommandPool, nullptr);

    vkDestroyDevice(mDevice, nullptr);

    Util::destroyDebugReportCallbackEXT(mInstance, mDebugCallback, nullptr);

    vkDestroySurfaceKHR(mInstance, mSurface, nullptr);
    vkDestroyInstance(mInstance, nullptr);

    glfwDestroyWindow(mWindow);
    glfwTerminate();
}

void TutorialApp::createInstance()
{
    if (VALIDATION_LAYERS_ENABLE && !checkValidationLayers())
    {
        throw std::runtime_error("Validation layers are enabled but not available!");
    }

    // Basic application information.
    VkApplicationInfo appInfo{};
    appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    appInfo.pApplicationName = WINDOW_TITLE;
    appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
    appInfo.pEngineName = "No Engine";
    appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
    appInfo.apiVersion = VK_API_VERSION_1_0;

    auto requiredExtensions = getRequiredExtensions();

    VkInstanceCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    createInfo.pApplicationInfo = &appInfo;
    // Extensions which are required to run the application.
    createInfo.enabledExtensionCount = static_cast<uint32_t>(requiredExtensions.size());
    createInfo.ppEnabledExtensionNames = requiredExtensions.data();
    if (VALIDATION_LAYERS_ENABLE)
    {
        createInfo.enabledLayerCount = static_cast<uint32_t>(VALIDATION_LAYERS.size());
        createInfo.ppEnabledLayerNames = VALIDATION_LAYERS.data();
    }
    else
    {
        createInfo.enabledLayerCount = 0;
        createInfo.ppEnabledLayerNames = nullptr;
    }

    if (vkCreateInstance(&createInfo, nullptr, &mInstance) != VK_SUCCESS)
    {
        throw std::runtime_error("Failed to create Vulkan instance.");
    }
}

void TutorialApp::setupDebugCallback()
{
    if (!VALIDATION_LAYERS_ENABLE)
    {
        return;
    }

    // Callback for warning and error messages.
    VkDebugReportCallbackCreateInfoEXT createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT;
    createInfo.flags = VK_DEBUG_REPORT_ERROR_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT;
    createInfo.pfnCallback = Util::uvkDebugCallback;

    if (Util::createDebugReportCallbackEXT(mInstance, &createInfo, nullptr, &mDebugCallback) != VK_SUCCESS)
    {
        throw std::runtime_error("Unable to set debug report callback!");
    }
}

void TutorialApp::createSurface()
{
    if (glfwCreateWindowSurface(mInstance, mWindow, nullptr, &mSurface) != VK_SUCCESS)
    {
        throw std::runtime_error("Unable to create Vulkan window surface!");
    }
}

void TutorialApp::pickPhysicalDevice()
{
    // Get number of devices.
    uint32_t deviceCount{0};
    vkEnumeratePhysicalDevices(mInstance, &deviceCount, nullptr);
    if (deviceCount == 0)
    {
        throw std::runtime_error("No suitable devices found!");
    }

    // Get the actual devices.
    std::vector<VkPhysicalDevice> devices(deviceCount);
    vkEnumeratePhysicalDevices(mInstance, &deviceCount, devices.data());

    // Choose the preferred device.
    for (const auto &device : devices)
    {
        if (isDeviceSuitable(device))
        {
            mPhysicalDevice = device;
            break;
        }
    }

    if (mPhysicalDevice == VK_NULL_HANDLE)
    {
        throw std::runtime_error("No suitable devices found!");
    }
}

void TutorialApp::createLogicalDevice()
{
    auto indices = findQueueFamilies(mPhysicalDevice);

    // Vector of queue creation information.
    std::vector<VkDeviceQueueCreateInfo> queueCreateInfos;
    // Set of queue indices, which will be used.
    std::set<int> queueFamilyIndices = {indices.graphicsFamily, indices.presentFamily};

    float queuePriority{1.0f};
    for (auto familyIndex : queueFamilyIndices)
    { // For each required queue family.
        VkDeviceQueueCreateInfo qCreateInfo{};
        qCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
        qCreateInfo.queueFamilyIndex = static_cast<uint32_t>(familyIndex);
        qCreateInfo.queueCount = 1;
        qCreateInfo.pQueuePriorities = &queuePriority;
        queueCreateInfos.emplace_back(qCreateInfo);
    }

    VkDeviceCreateInfo dCreateInfo{};
    dCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    dCreateInfo.pQueueCreateInfos = queueCreateInfos.data();
    dCreateInfo.queueCreateInfoCount = static_cast<uint32_t>(queueCreateInfos.size());

    // TODO - Select required features.
    VkPhysicalDeviceFeatures deviceFeatures{};
    deviceFeatures.samplerAnisotropy = VK_TRUE;

    dCreateInfo.pEnabledFeatures = &deviceFeatures;

    dCreateInfo.enabledExtensionCount = static_cast<uint32_t>(DEVICE_EXTENSIONS.size());
    dCreateInfo.ppEnabledExtensionNames = DEVICE_EXTENSIONS.data();

    if (VALIDATION_LAYERS_ENABLE)
    {
        dCreateInfo.enabledLayerCount = static_cast<uint32_t>(VALIDATION_LAYERS.size());
        dCreateInfo.ppEnabledLayerNames = VALIDATION_LAYERS.data();
    }
    else
    {
        dCreateInfo.enabledLayerCount = 0;
    }

    if (vkCreateDevice(mPhysicalDevice, &dCreateInfo, nullptr, &mDevice) != VK_SUCCESS)
    {
        throw std::runtime_error("Unable to create logical device!");
    }

    vkGetDeviceQueue(mDevice, static_cast<uint32_t>(indices.graphicsFamily), 0, &mGraphicsQueue);
    vkGetDeviceQueue(mDevice, static_cast<uint32_t>(indices.presentFamily), 0, &mPresentQueue);
}

void TutorialApp::createSwapChain()
{
    SwapChainSupportDetails details = querySwapChainDetails(mPhysicalDevice);

    auto surfaceFormat = chooseSwapSurfaceFormat(details.formats);
    auto presentMode = chooseSwapPresentMode(details.presentModes);
    auto extent = chooseSwapExtent(details.capabilities);

    // Choose a number of images in the swap chain. +1 for triple buffering.
    uint32_t imageCount{details.capabilities.minImageCount + 1};
    //  If there is a limit.
    if (details.capabilities.maxImageCount > 0)
    { // Then be sure, we are within the limit.
        imageCount = std::min(imageCount, details.capabilities.maxImageCount);
    }

    // Creation of the swap chain begins...
    VkSwapchainCreateInfoKHR createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    createInfo.surface = mSurface;

    // Setup images in the swap chain.
    createInfo.minImageCount = imageCount;
    createInfo.imageFormat = surfaceFormat.format;
    createInfo.imageColorSpace = surfaceFormat.colorSpace;
    createInfo.imageExtent = extent;
    // Not a stereoscopic-3D application.
    createInfo.imageArrayLayers = 1;
    createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
    // TODO - Add old swap chain, when recreating.
    // Choose image sharing method for multiple queues.
    auto indices = findQueueFamilies(mPhysicalDevice);
    // TODO - Ugly...
    auto arr = {static_cast<uint32_t>(indices.graphicsFamily), static_cast<uint32_t>(indices.presentFamily)};

    if (indices.graphicsFamily != indices.presentFamily)
    { // We will get 2 different queues, which means, we draw in parallel.
        createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
        // Which queues will share this swap chain.
        createInfo.queueFamilyIndexCount = static_cast<uint32_t>(arr.size());
        createInfo.pQueueFamilyIndices = arr.begin();
    }
    else
    { // We will get only one queue.
        createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
        // Since no sharing will be realized, no queues are specified.
        createInfo.queueFamilyIndexCount = 0u;
        createInfo.pQueueFamilyIndices = nullptr;
    }

    createInfo.preTransform = details.capabilities.currentTransform;
    // Ignore alpha channel.
    createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
    createInfo.presentMode = presentMode;
    // Enable discarding non-visible operations.
    createInfo.clipped = VK_TRUE;
    createInfo.oldSwapchain = mSwapChain;

    if (vkCreateSwapchainKHR(mDevice, &createInfo, nullptr, &mSwapChain) != VK_SUCCESS)
    {
        throw std::runtime_error("Unable to create swap chain!");
    }

    // If we recreated swap chain, delete the old one.
    if (createInfo.oldSwapchain != VK_NULL_HANDLE)
    {
        vkDestroySwapchainKHR(mDevice, createInfo.oldSwapchain, nullptr);
    }

    /**
     * Get the images contained inside swap chain.
     * It is important to check the actual number of images, because the
     * number can be greater than the number of requested images.
     */
    vkGetSwapchainImagesKHR(mDevice, mSwapChain, &imageCount, nullptr);
    mSwapChainImages.resize(imageCount);
    vkGetSwapchainImagesKHR(mDevice, mSwapChain, &imageCount, mSwapChainImages.data());

    mSwapChainFormat = surfaceFormat.format;
    mSwapChainExtent = extent;
}

void TutorialApp::createSwapChainViews()
{
    // Make space for the image views.
    mSwapChainViews.resize(mSwapChainImages.size());

    for (uint32_t iii = 0; iii < mSwapChainViews.size(); ++iii)
    {
        createImageView(mSwapChainImages[iii], mSwapChainFormat,
                        VK_IMAGE_ASPECT_COLOR_BIT, mSwapChainViews[iii]);
    }
}

void TutorialApp::createMSAAImageView()
{
    createImage(
            mSwapChainExtent.width, mSwapChainExtent.height, mSwapChainFormat,
            VK_SAMPLE_COUNT_4_BIT, VK_IMAGE_TILING_OPTIMAL,
            VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT,
            VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, mMSAAImage, mMSAAImageMemory);
    createImageView(mMSAAImage, mSwapChainFormat, VK_IMAGE_ASPECT_COLOR_BIT, mMSAAImageView);
}

void TutorialApp::createDepthImageView()
{
    VkFormat depthFormat = findDepthFormat();

    // Multisampled depth buffer.
    createImage(
            mSwapChainExtent.width, mSwapChainExtent.height, depthFormat,
            VK_SAMPLE_COUNT_4_BIT, VK_IMAGE_TILING_OPTIMAL,
            VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
            VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, mMSAADepthImage, mMSAADepthImageMemory);
    // TODO - Add ...STENCIL_BIT.
    createImageView(mMSAADepthImage, depthFormat, VK_IMAGE_ASPECT_DEPTH_BIT, mMSAADepthImageView);

    // Resolved depth buffer.
    createImage(
            mSwapChainExtent.width, mSwapChainExtent.height, depthFormat,
            VK_SAMPLE_COUNT_1_BIT, VK_IMAGE_TILING_OPTIMAL,
            VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
            VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, mDepthImage, mDepthImageMemory);
    // TODO - Add ...STENCIL_BIT.
    createImageView(mDepthImage, depthFormat, VK_IMAGE_ASPECT_DEPTH_BIT, mDepthImageView);

    transitionImageLayout(
            mDepthImage, depthFormat,
            VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL);
}

void TutorialApp::createRenderPass()
{
    // Multisampled render target.
    VkAttachmentDescription msaaColorAtt{};
    msaaColorAtt.format = mSwapChainFormat;
    msaaColorAtt.samples = VK_SAMPLE_COUNT_4_BIT;
    // Clear the attachment before pass.
    msaaColorAtt.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    // Explicitly store the color values.
    msaaColorAtt.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    // We don't use the stencil buffer.
    msaaColorAtt.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    msaaColorAtt.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    // Image layout before render pass, which we don't need to specify.
    msaaColorAtt.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    // Image layout after render pass, we will be presenting it in the swap chain.
    msaaColorAtt.finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    // Resolved multisampled image for presentation.
    VkAttachmentDescription colorAtt{};
    colorAtt.format = mSwapChainFormat;
    colorAtt.samples = VK_SAMPLE_COUNT_1_BIT;
    // Clear the attachment before pass.
    colorAtt.loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    // Explicitly store the color values.
    colorAtt.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    // We don't use the stencil buffer.
    colorAtt.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    colorAtt.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    // Image layout before render pass, which we don't need to specify.
    colorAtt.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    // Image layout after render pass, we will be presenting it in the swap chain.
    colorAtt.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

    // Multisampled depth buffer.
    VkAttachmentDescription msaaDepthAtt{};
    msaaDepthAtt.format = findDepthFormat();
    msaaDepthAtt.samples = VK_SAMPLE_COUNT_4_BIT;
    msaaDepthAtt.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    msaaDepthAtt.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    msaaDepthAtt.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    msaaDepthAtt.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    msaaDepthAtt.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    msaaDepthAtt.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

    // Resolved depth buffer.
    VkAttachmentDescription depthAtt{};
    depthAtt.format = findDepthFormat();
    depthAtt.samples = VK_SAMPLE_COUNT_1_BIT;
    depthAtt.loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    depthAtt.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    depthAtt.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    depthAtt.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    depthAtt.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    depthAtt.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

    auto attachments = {
            msaaColorAtt,
            colorAtt,
            msaaDepthAtt,
            depthAtt
    };

    /**
     * For now render pass will contain only one subpass.
     */

    // Dependency, which waits for the destination image to be ready.
    VkSubpassDependency imageReadyDep{};
    // Dependency is at the top.
    imageReadyDep.srcSubpass = VK_SUBPASS_EXTERNAL;
    // The first subpass is the next in line.
    imageReadyDep.dstSubpass = 0;
    /*
     * Wait for the color attachment to be ready.
     * Dependency should wait for all commands in the color attachment
     * stage to finish, before continuing.
     */
    imageReadyDep.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    imageReadyDep.srcAccessMask = 0;
    /*
     * Operations in the next stage should wait for this dependency, if
     * they are in the color attachment stage, which read or write from/to the
     * color attachment.
     */
    imageReadyDep.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    imageReadyDep.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT |
                                  VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
    imageReadyDep.dependencyFlags = 0;

    // ### First subpass, rendering into colormap. ###

    // MSAA target color buffer.
    VkAttachmentReference colorAttMSAARef{};
    colorAttMSAARef.attachment = 0;
    colorAttMSAARef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    // Resolve color buffer.
    VkAttachmentReference colorAttRef{};
    colorAttRef.attachment = 1;
    colorAttRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    // Multisampled depth buffer.
    VkAttachmentReference msaaDepthAttRef{};
    msaaDepthAttRef.attachment = 2;
    msaaDepthAttRef.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

    VkSubpassDescription drawSubpassDesc{};
    // We are using graphics shaders, not compute.
    drawSubpassDesc.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
    // No input attachments.
    drawSubpassDesc.inputAttachmentCount = 0;
    drawSubpassDesc.pInputAttachments = nullptr;
    // Use 1 color attachment.
    drawSubpassDesc.colorAttachmentCount = 1;
    drawSubpassDesc.pColorAttachments = &colorAttMSAARef;
    // Rest is unused.
    drawSubpassDesc.pResolveAttachments = &colorAttRef;
    drawSubpassDesc.pDepthStencilAttachment = &msaaDepthAttRef;
    drawSubpassDesc.preserveAttachmentCount = 0;
    drawSubpassDesc.pPreserveAttachments = nullptr;

    // ### End of first subpass. ###

    VkRenderPassCreateInfo renderPassCI{};
    renderPassCI.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
    // Bind the main color attachment, for ease of use.
    renderPassCI.attachmentCount = attachments.size();
    renderPassCI.pAttachments = attachments.begin();
    // Use the draw subpass.
    renderPassCI.subpassCount = 1;
    renderPassCI.pSubpasses = &drawSubpassDesc;
    // Add the dependency waiting for image to be ready.
    renderPassCI.dependencyCount = 1;
    renderPassCI.pDependencies = &imageReadyDep;

    if (vkCreateRenderPass(mDevice, &renderPassCI, nullptr, &mRenderPass) != VK_SUCCESS)
    {
        throw std::runtime_error("Unable to create render pass!");
    }
}

void TutorialApp::createDescriptorSetLayout()
{
    // Layout for the uniform buffer object.
    VkDescriptorSetLayoutBinding uboLb{};
    uboLb.binding = 0;
    // Only one uniform, ...UNIFORM_DYNAMIC is used for multiple uniforms.
    uboLb.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    uboLb.descriptorCount = 1;
    // Used only in the vertex shader.
    uboLb.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
    uboLb.pImmutableSamplers = nullptr;

    // Layout for the texture sampler.
    VkDescriptorSetLayoutBinding texSamplerLb{};
    texSamplerLb.binding = 1;
    // Combined texture sampler.
    texSamplerLb.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    texSamplerLb.descriptorCount = 1;
    // Used only in the fragment shader.
    texSamplerLb.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
    texSamplerLb.pImmutableSamplers = nullptr;

    auto bindings = {
            uboLb,
            texSamplerLb
    };

    VkDescriptorSetLayoutCreateInfo layoutCI{};
    layoutCI.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    layoutCI.bindingCount = bindings.size();
    layoutCI.pBindings = bindings.begin();

    if (vkCreateDescriptorSetLayout(mDevice, &layoutCI, nullptr, &mDescriptorSetLayout) != VK_SUCCESS)
    {
        throw std::runtime_error("Unable to create descriptor set layout!");
    }
}

void TutorialApp::createGraphicsPipeline()
{
    // Load shader Spir-V code.
    auto vertSpirV = Util::loadFileB(std::string(SHADER_FOLDER) + SHADER_NAME + SHADER_VERT_EXT + SHADER_SPIRV_EXT);
    auto fragSpirV = Util::loadFileB(std::string(SHADER_FOLDER) + SHADER_NAME + SHADER_FRAG_EXT + SHADER_SPIRV_EXT);

    // Create required shader modules.
    auto vertShaderModule = createShaderModule(vertSpirV);
    auto fragShaderModule = createShaderModule(fragSpirV);

    // Vertex shader stage creation.
    VkPipelineShaderStageCreateInfo vertShaderStageCI{};
    vertShaderStageCI.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    vertShaderStageCI.stage = VK_SHADER_STAGE_VERTEX_BIT;
    vertShaderStageCI.module = vertShaderModule;
    // Main function name.
    vertShaderStageCI.pName = "main";
    // No constant specialization.
    vertShaderStageCI.pSpecializationInfo = nullptr;

    // Fragment shader stage creation.
    VkPipelineShaderStageCreateInfo fragShaderStageCI{};
    fragShaderStageCI.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    fragShaderStageCI.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
    fragShaderStageCI.module = fragShaderModule;
    // Main function name.
    fragShaderStageCI.pName = "main";
    // No constant specialization.
    fragShaderStageCI.pSpecializationInfo = nullptr;

    auto shaderStages = {vertShaderStageCI, fragShaderStageCI};

    // Vertex puller settings.
    VkPipelineVertexInputStateCreateInfo vertInputCI{};
    vertInputCI.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;

    auto bd = SimpleVertex::getBindingDescription();
    auto ad = SimpleVertex::getAttributeDescriptions();

    vertInputCI.vertexBindingDescriptionCount = 1;
    vertInputCI.pVertexBindingDescriptions = &bd;
    vertInputCI.vertexAttributeDescriptionCount = static_cast<uint32_t>(ad.size());
    vertInputCI.pVertexAttributeDescriptions = ad.data();

    // Primitive Assembly settings.
    VkPipelineInputAssemblyStateCreateInfo assemblyCI{};
    assemblyCI.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    assemblyCI.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
    assemblyCI.primitiveRestartEnable = VK_FALSE;

    // Setup the main viewport.
    // Upper left corner.
    mViewport.x = 0;
    mViewport.y = 0;
    mViewport.width = mSwapChainExtent.width;
    mViewport.height = mSwapChainExtent.height;
    mViewport.minDepth = 0.0f;
    mViewport.maxDepth = 1.0f;

    // Viewport scissoring.
    VkRect2D vpScissor{};
    vpScissor.offset = {0, 0};
    vpScissor.extent = mSwapChainExtent;

    // Combine viewport and scissor into viewport state.
    VkPipelineViewportStateCreateInfo vpStateCI{};
    vpStateCI.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    vpStateCI.viewportCount = 1;
    vpStateCI.pViewports = &mViewport;
    vpStateCI.scissorCount = 1;
    vpStateCI.pScissors = &vpScissor;

    // Rasterization stage.
    VkPipelineRasterizationStateCreateInfo rasterizationCI{};
    rasterizationCI.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    // Do not clamp to far/near plane. Useful for shadow mapping.
    rasterizationCI.depthClampEnable = VK_FALSE;
    // Enable rasterization stage.
    rasterizationCI.rasterizerDiscardEnable = VK_FALSE;
    // Fill polygons, VK_POLYGON_MODE_LINE for wireframe.
    rasterizationCI.polygonMode = VK_POLYGON_MODE_FILL;
    // Cull the back faces of polygons.
    rasterizationCI.cullMode = VK_CULL_MODE_BACK_BIT;
    // Vertices for front should be in clockwise order.
    rasterizationCI.frontFace = VK_FRONT_FACE_CLOCKWISE;
    // Don't do depth biasing. Useful for shadow mapping.
    rasterizationCI.depthBiasEnable = VK_FALSE;
    rasterizationCI.depthBiasConstantFactor = 0.0f;
    rasterizationCI.depthBiasClamp = 0.0f;
    rasterizationCI.depthBiasSlopeFactor = 0.0f;
    // Normal line width. Wider lines require wideLines GPU feature.
    rasterizationCI.lineWidth = 1.0f;

    // Multisampling settings.
    VkPipelineMultisampleStateCreateInfo multisampleCI{};
    multisampleCI.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    // TODO - Enable multisampling.
    multisampleCI.rasterizationSamples = VK_SAMPLE_COUNT_4_BIT;
    multisampleCI.sampleShadingEnable = VK_FALSE;
    multisampleCI.minSampleShading = 1.0f;
    multisampleCI.pSampleMask = nullptr;
    multisampleCI.alphaToCoverageEnable = VK_FALSE;
    multisampleCI.alphaToOneEnable = VK_FALSE;

    // Settings for stencil/depth buffer.
    VkPipelineDepthStencilStateCreateInfo depthStencilCI{};
    depthStencilCI.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
    // Do depth testing.
    depthStencilCI.depthTestEnable = VK_TRUE;
    // Enable write access to the depth buffer.
    depthStencilCI.depthWriteEnable = VK_TRUE;
    // Check < depth.
    depthStencilCI.depthCompareOp = VK_COMPARE_OP_LESS;
    // No bounds checking.
    depthStencilCI.depthBoundsTestEnable = VK_FALSE;
    // No stencil buffer.
    depthStencilCI.stencilTestEnable = VK_FALSE;
    depthStencilCI.front = {};
    depthStencilCI.back = {};
    // Depth from 0.0 (closest) to 1.0 (furthest).
    depthStencilCI.minDepthBounds = 0.0f;
    depthStencilCI.maxDepthBounds = 1.0f;

    // Color blending settings for the main framebuffer.
    VkPipelineColorBlendAttachmentState colorBlendAtt{};
    // No color blending.
    colorBlendAtt.blendEnable = VK_FALSE;
    colorBlendAtt.srcColorBlendFactor = VK_BLEND_FACTOR_ONE;
    colorBlendAtt.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;
    colorBlendAtt.colorBlendOp = VK_BLEND_OP_ADD;
    colorBlendAtt.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
    colorBlendAtt.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
    colorBlendAtt.alphaBlendOp = VK_BLEND_OP_ADD;
    colorBlendAtt.colorWriteMask = VK_COLOR_COMPONENT_R_BIT |
                                   VK_COLOR_COMPONENT_G_BIT |
                                   VK_COLOR_COMPONENT_B_BIT |
                                   VK_COLOR_COMPONENT_A_BIT;

    // Color blending global settings.
    VkPipelineColorBlendStateCreateInfo colorBlendCI{};
    colorBlendCI.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    // No logical operations required.
    colorBlendCI.logicOpEnable = VK_FALSE;
    colorBlendCI.logicOp = VK_LOGIC_OP_COPY;
    // Only one color attachment.
    colorBlendCI.attachmentCount = 1;
    colorBlendCI.pAttachments = &colorBlendAtt;
    // No blending.
    colorBlendCI.blendConstants[0] = 0.0f;
    colorBlendCI.blendConstants[1] = 0.0f;
    colorBlendCI.blendConstants[2] = 0.0f;
    colorBlendCI.blendConstants[3] = 0.0f;

    // Select attributes which can be changed dynamically.
    VkPipelineDynamicStateCreateInfo dynamicStateCI{};
    dynamicStateCI.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
    static constexpr uint32_t NUM_DYNAMIC_STATES{2};
    VkDynamicState dynamicStates[NUM_DYNAMIC_STATES] = {
        // Changing resolution of the viewport.
        VK_DYNAMIC_STATE_VIEWPORT,
        // Changing line width.
        VK_DYNAMIC_STATE_LINE_WIDTH};
    dynamicStateCI.dynamicStateCount = NUM_DYNAMIC_STATES;
    dynamicStateCI.pDynamicStates = dynamicStates;

    // Pipeline layout and uniform mapping.
    VkPipelineLayoutCreateInfo pipelineLayoutCI{};
    pipelineLayoutCI.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    // Use the main descriptor set.
    pipelineLayoutCI.setLayoutCount = 1;
    pipelineLayoutCI.pSetLayouts = &mDescriptorSetLayout;
    pipelineLayoutCI.pushConstantRangeCount = 0;
    pipelineLayoutCI.pPushConstantRanges = nullptr;

    if (vkCreatePipelineLayout(mDevice, &pipelineLayoutCI, nullptr, &mPipelineLayout) != VK_SUCCESS)
    {
        throw std::runtime_error("Unable to create pipeline layout!");
    }

    // Create the final graphics pipeline.
    VkGraphicsPipelineCreateInfo graphicsPipelineCI{};
    graphicsPipelineCI.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    // Use created shader stages.
    graphicsPipelineCI.stageCount = static_cast<uint32_t>(shaderStages.size());
    graphicsPipelineCI.pStages = shaderStages.begin();
    // Set information provided by already created structures
    graphicsPipelineCI.pVertexInputState = &vertInputCI;
    graphicsPipelineCI.pInputAssemblyState = &assemblyCI;
    graphicsPipelineCI.pTessellationState = nullptr;
    graphicsPipelineCI.pViewportState = &vpStateCI;
    graphicsPipelineCI.pRasterizationState = &rasterizationCI;
    graphicsPipelineCI.pMultisampleState = &multisampleCI;
    graphicsPipelineCI.pDepthStencilState = &depthStencilCI;
    graphicsPipelineCI.pColorBlendState = &colorBlendCI;
    graphicsPipelineCI.pDynamicState = &dynamicStateCI;
    // Set already created Vulkan objects.
    graphicsPipelineCI.layout = mPipelineLayout;
    graphicsPipelineCI.renderPass = mRenderPass;
    // Use the first subpass = draw subpass.
    graphicsPipelineCI.subpass = 0;
    // This is the first graphics pipeline -> no derivative.
    graphicsPipelineCI.basePipelineHandle = VK_NULL_HANDLE;
    graphicsPipelineCI.basePipelineIndex = -1;

    if (vkCreateGraphicsPipelines(mDevice, VK_NULL_HANDLE, 1, &graphicsPipelineCI, nullptr, &mPipeline) != VK_SUCCESS)
    {
        throw std::runtime_error("Unable to create graphics pipeline!");
    }

    vkDestroyShaderModule(mDevice, fragShaderModule, nullptr);
    vkDestroyShaderModule(mDevice, vertShaderModule, nullptr);
}

void TutorialApp::createFramebuffers()
{
    mSwapChainFramebuffers.resize(mSwapChainViews.size());

    // Create a framebuffer for each of the images in the swap chain.
    for (std::size_t iii = 0; iii < mSwapChainFramebuffers.size(); ++iii)
    {
        auto attachments = {
            mMSAAImageView,
            mSwapChainViews[iii],
            mMSAADepthImageView,
            mDepthImageView
        };

        VkFramebufferCreateInfo fbCI{};
        fbCI.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
        fbCI.renderPass = mRenderPass;
        fbCI.attachmentCount = static_cast<uint32_t>(attachments.size());
        fbCI.pAttachments = attachments.begin();
        fbCI.width = mSwapChainExtent.width;
        fbCI.height = mSwapChainExtent.height;
        fbCI.layers = 1;

        if (vkCreateFramebuffer(mDevice, &fbCI, nullptr, &mSwapChainFramebuffers[iii]) != VK_SUCCESS)
        {
            throw std::runtime_error("Unable to create framebuffer");
        }
    }
}

void TutorialApp::createCommandPool()
{
    auto indices = findQueueFamilies(mPhysicalDevice);

    // Create command pool for the graphics queue family.
    VkCommandPoolCreateInfo commandPoolCI{};
    commandPoolCI.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    commandPoolCI.flags = 0;
    commandPoolCI.queueFamilyIndex = static_cast<uint32_t>(indices.graphicsFamily);

    if (vkCreateCommandPool(mDevice, &commandPoolCI, nullptr, &mCommandPool) != VK_SUCCESS)
    {
        throw std::runtime_error("Unable to create command pool!");
    }
}

void TutorialApp::createTextureImage()
{
    static constexpr uint32_t IMG_WIDTH{512};
    static constexpr uint32_t IMG_HEIGHT{512};
    static constexpr uint32_t IMG_CHANNELS{4};
    uint8_t *pixels = new uint8_t[IMG_WIDTH * IMG_HEIGHT * IMG_CHANNELS];
    for (uint32_t yyy = 0; yyy < IMG_HEIGHT; ++yyy)
    {
        for (uint32_t xxx = 0; xxx < IMG_WIDTH; ++xxx)
        {
            pixels[yyy * IMG_WIDTH * IMG_CHANNELS + xxx * IMG_CHANNELS + 0] = (xxx + yyy) % 255;
            pixels[yyy * IMG_WIDTH * IMG_CHANNELS + xxx * IMG_CHANNELS + 1] = (xxx + yyy) % 255;
            pixels[yyy * IMG_WIDTH * IMG_CHANNELS + xxx * IMG_CHANNELS + 2] = (xxx + yyy) % 255;
            pixels[yyy * IMG_WIDTH * IMG_CHANNELS + xxx * IMG_CHANNELS + 3] = 255;
        }
    }

    // Create the staging buffer.
    VkDeviceSize imageSize = IMG_WIDTH * IMG_HEIGHT * IMG_CHANNELS;
    createBuffer(
            imageSize,
            VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
            VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
            mStagingBuffer, mStagingBufferMemory);

    // Copy image to the buffer.
    void *bufferData{nullptr};
    vkMapMemory(mDevice, mStagingBufferMemory, 0, imageSize, 0, &bufferData);
    {
        memcpy(bufferData, pixels, imageSize);
    }
    vkUnmapMemory(mDevice, mStagingBufferMemory);

    // Free the image memory.
    delete[] pixels;

    // Create the texture image used for sampling.
    createImage(
            // Normalized texture image.
            IMG_WIDTH, IMG_HEIGHT, VK_FORMAT_R8G8B8A8_UNORM,
            VK_SAMPLE_COUNT_1_BIT, VK_IMAGE_TILING_OPTIMAL,
            /*
             * Data will be copied from the staging buffer into this
             * image and then sampled in the shader.
             */
            VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
            VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, mTextureImage, mTextureImageMemory);

    // Change the layout to the optimal on for copying into.
    transitionImageLayout(
            mTextureImage, VK_FORMAT_R8G8B8A8_UNORM,
            VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
    // Copy data from staging buffer to the image.
    copyBufferToImage(mStagingBuffer, mTextureImage, IMG_WIDTH, IMG_HEIGHT);
    // Change the layout to optimal one for sampling in the shader.
    transitionImageLayout(
            mTextureImage, VK_FORMAT_R8G8B8A8_UNORM,
            VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

    // Free the staging buffer.
    vkDestroyBuffer(mDevice, mStagingBuffer, nullptr);
    vkFreeMemory(mDevice, mStagingBufferMemory, nullptr);
}

void TutorialApp::createTextureSampler()
{
    VkSamplerCreateInfo samplerCI{};
    samplerCI.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
    samplerCI.flags = 0;

    samplerCI.magFilter = VK_FILTER_LINEAR;
    samplerCI.minFilter = VK_FILTER_LINEAR;

    samplerCI.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    samplerCI.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    samplerCI.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;

    samplerCI.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
    samplerCI.mipLodBias = 0.0f;
    samplerCI.minLod = 0.0f;
    samplerCI.maxLod = 0.0f;

    samplerCI.compareEnable = VK_FALSE;
    samplerCI.compareOp = VK_COMPARE_OP_ALWAYS;

    samplerCI.anisotropyEnable = VK_TRUE;
    samplerCI.maxAnisotropy = 16.0f;
    samplerCI.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
    samplerCI.unnormalizedCoordinates = VK_FALSE;

    if (vkCreateSampler(mDevice, &samplerCI, nullptr, &mTextureSampler) != VK_SUCCESS)
    {
        throw std::runtime_error("Unable to create texture sampler!");
    }
}

void TutorialApp::createTextureImageView()
{
    createImageView(
            mTextureImage,
            VK_FORMAT_R8G8B8A8_UNORM,
            VK_IMAGE_ASPECT_COLOR_BIT,
            mTextureImageView);
}

void TutorialApp::createVertexBuffer()
{
    // Create a staging buffer for the vertex buffer.
    auto bufferSize = static_cast<VkDeviceSize>(sizeof(ExampleData::vertices[0]) * ExampleData::vertices.size());
    VkBuffer stagingBuffer;
    VkDeviceMemory stagingMemory;
    createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                 VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
                 stagingBuffer, stagingMemory);

    // Map the GPU memory into the system (CPU) memory.
    void *data{nullptr};
    vkMapMemory(mDevice, stagingMemory, 0, bufferSize, 0, &data);
    {
        memcpy(data, ExampleData::vertices.data(), static_cast<std::size_t>(bufferSize));
    }
    vkUnmapMemory(mDevice, stagingMemory);

    // Create the actual vertex buffer.
    createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
                 VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, mVertexBuffer, mVertexMemory);
    copyBuffer(stagingBuffer, mVertexBuffer, bufferSize);

    vkDestroyBuffer(mDevice, stagingBuffer, nullptr);
    vkFreeMemory(mDevice, stagingMemory, nullptr);
}

void TutorialApp::createIndexBuffer()
{
    // Create a staging buffer for the index buffer.
    auto bufferSize = static_cast<VkDeviceSize>(sizeof(ExampleData::indices[0]) * ExampleData::indices.size());
    VkBuffer stagingBuffer;
    VkDeviceMemory stagingMemory;
    createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                 VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
                 stagingBuffer, stagingMemory);

    // Map the GPU memory into the system (CPU) memory.
    void *data{nullptr};
    vkMapMemory(mDevice, stagingMemory, 0, bufferSize, 0, &data);
    {
        memcpy(data, ExampleData::indices.data(), static_cast<std::size_t>(bufferSize));
    }
    vkUnmapMemory(mDevice, stagingMemory);

    // Create the actual index buffer.
    createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
                 VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, mIndexBuffer, mIndexMemory);
    copyBuffer(stagingBuffer, mIndexBuffer, bufferSize);

    vkDestroyBuffer(mDevice, stagingBuffer, nullptr);
    vkFreeMemory(mDevice, stagingMemory, nullptr);
}

void TutorialApp::createUniformBuffer()
{
    auto bufferSize = static_cast<VkDeviceSize>(sizeof(UniformBufferObject));
    createBuffer(bufferSize, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
                 VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
                 mUniformBuffer, mUniformMemory);
}

void TutorialApp::createDescriptorPool()
{
    // Create a pool for the one Uniform buffer descriptor.
    VkDescriptorPoolSize uniformPS{};
    uniformPS.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    uniformPS.descriptorCount = 1;

    // Create a second pool for texture samplers.
    VkDescriptorPoolSize samplerPS{};
    samplerPS.type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    samplerPS.descriptorCount = 1;

    auto poolSizes = {
            uniformPS,
            samplerPS
    };

    VkDescriptorPoolCreateInfo poolCI{};
    poolCI.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    // Allow creating and freeing descriptor sets.
    poolCI.flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;
    poolCI.maxSets = 1;
    poolCI.poolSizeCount = poolSizes.size();
    poolCI.pPoolSizes = poolSizes.begin();

    if (vkCreateDescriptorPool(mDevice, &poolCI, nullptr, &mDescriptorPool) != VK_SUCCESS)
    {
        throw std::runtime_error("Unable to create descriptor pool!");
    }
}

void TutorialApp::createDescriptorSet()
{
    // Allocate descriptor set for the uniforms.
    auto layouts = { mDescriptorSetLayout };
    VkDescriptorSetAllocateInfo descriptorAI{};
    descriptorAI.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    descriptorAI.descriptorPool =  mDescriptorPool;
    descriptorAI.descriptorSetCount = static_cast<uint32_t>(layouts.size());
    descriptorAI.pSetLayouts = layouts.begin();

    if (vkAllocateDescriptorSets(mDevice, &descriptorAI, &mDescriptorSet) != VK_SUCCESS)
    {
        throw std::runtime_error("Unable to allocate descriptor set!");
    }

    // Connect allocated descriptor set with prepared uniform object buffer.
    VkDescriptorBufferInfo uboBI{};
    uboBI.buffer = mUniformBuffer;
    uboBI.offset = 0;
    uboBI.range = sizeof(UniformBufferObject);
    VkWriteDescriptorSet uboWDS{};
    uboWDS.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    uboWDS.dstSet = mDescriptorSet;
    uboWDS.dstBinding = 0;
    uboWDS.dstArrayElement = 0;
    uboWDS.descriptorCount = 1;
    uboWDS.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    uboWDS.pImageInfo = nullptr;
    uboWDS.pBufferInfo = &uboBI;
    uboWDS.pTexelBufferView = nullptr;

    // Connect descriptor to the sampler.
    VkDescriptorImageInfo samplerII{};
    samplerII.sampler = mTextureSampler;
    samplerII.imageView = mTextureImageView;
    samplerII.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    VkWriteDescriptorSet samplerWDS{};
    samplerWDS.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    samplerWDS.dstSet = mDescriptorSet;
    samplerWDS.dstBinding = 1;
    samplerWDS.dstArrayElement = 0;
    samplerWDS.descriptorCount = 1;
    samplerWDS.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    samplerWDS.pImageInfo = &samplerII;
    samplerWDS.pBufferInfo = nullptr;
    samplerWDS.pTexelBufferView = nullptr;

    auto writers = {
            uboWDS,
            samplerWDS
    };

    // Update settings of the descriptor set.
    vkUpdateDescriptorSets(mDevice, writers.size(), writers.begin(), 0, nullptr);
}

void TutorialApp::createCommandBuffers()
{
    mCommandBuffers.resize(mSwapChainFramebuffers.size());

    // Create the main command buffer.
    VkCommandBufferAllocateInfo commandBufferAI{};
    commandBufferAI.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    commandBufferAI.commandPool = mCommandPool;
    // Primary command buffer.
    commandBufferAI.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    // One buffer for each framebuffer.
    commandBufferAI.commandBufferCount = static_cast<uint32_t>(mCommandBuffers.size());

    if (vkAllocateCommandBuffers(mDevice, &commandBufferAI, mCommandBuffers.data()) != VK_SUCCESS)
    {
        throw std::runtime_error("Unable to allocate command buffers!");
    }

    for (std::size_t iii = 0; iii < mCommandBuffers.size(); ++iii)
    {
        VkCommandBufferBeginInfo commandBufferBI{};
        commandBufferBI.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        // Can be resubmitted to a queue when in pending state.
        commandBufferBI.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;
        commandBufferBI.pInheritanceInfo = nullptr;

        vkBeginCommandBuffer(mCommandBuffers[iii], &commandBufferBI);
        {
            // Start the render pass.
            std::array<VkClearValue, 4> clearColors{};
            // MSAA color buffer.
            clearColors[0].color = {0.0f, 0.0f, 0.0f, 1.0f};
            // Resolved color buffer.
            clearColors[1].color = {0.0f, 0.0f, 0.0f, 1.0f};
            // MSAA depth buffer.
            clearColors[2].depthStencil = {1.0f, 0};
            // Resolved depth buffer.
            clearColors[3].depthStencil = {1.0f, 0};
            VkRenderPassBeginInfo renderPassBI{};
            renderPassBI.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
            // Use the main render pass.
            renderPassBI.renderPass = mRenderPass;
            // Choose the framebuffer corresponding to this command buffer.
            renderPassBI.framebuffer = mSwapChainFramebuffers[iii];
            // Use whole frame space.
            renderPassBI.renderArea.extent = mSwapChainExtent;
            renderPassBI.renderArea.offset = {0, 0};
            // Select the clear color.
            renderPassBI.clearValueCount = clearColors.size();
            renderPassBI.pClearValues = clearColors.begin();

            // All of the render commands are in the buffer.
            vkCmdBeginRenderPass(mCommandBuffers[iii], &renderPassBI, VK_SUBPASS_CONTENTS_INLINE);

            // Bind the main pipeline, used in graphics mode.
            vkCmdBindPipeline(mCommandBuffers[iii], VK_PIPELINE_BIND_POINT_GRAPHICS, mPipeline);

            // Bind descriptor set containing uniform data.
            vkCmdBindDescriptorSets(mCommandBuffers[iii], VK_PIPELINE_BIND_POINT_GRAPHICS,
                                    mPipelineLayout, 0, 1, &mDescriptorSet, 0, nullptr);

            // TODO - Ugly.
            std::initializer_list<VkBuffer> buffers{
                mVertexBuffer
            };
            std::initializer_list<VkDeviceSize> offsets{
                0
            };
            // Bind the buffers required for drawing.
            vkCmdBindVertexBuffers(mCommandBuffers[iii], 0, static_cast<uint32_t>(buffers.size()),
                                   buffers.begin(), offsets.begin());

            // Bind the buffer containing vertex indices.
            vkCmdBindIndexBuffer(mCommandBuffers[iii], mIndexBuffer, 0, VK_INDEX_TYPE_UINT16);

            // Use the main viewport.
            vkCmdSetViewport(mCommandBuffers[iii], 0, 1, &mViewport);

            // TODO - Change to other objects than a single triangle.
            // Draw the 3 vertices of a triangle.
            //vkCmdDraw(mCommandBuffers[iii], static_cast<uint32_t>(ExampleData::vertices.size()), 1, 0, 0);
            vkCmdDrawIndexed(mCommandBuffers[iii], static_cast<uint32_t>(ExampleData::indices.size()), 1, 0, 0, 0);

            // End of the render pass.
            vkCmdEndRenderPass(mCommandBuffers[iii]);
        }
        if (vkEndCommandBuffer(mCommandBuffers[iii]) != VK_SUCCESS)
        {
            throw std::runtime_error("Unable to create command buffer!");
        }
    }
}

void TutorialApp::createSemaphores()
{
    VkSemaphoreCreateInfo semaphoreCI{};
    semaphoreCI.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

    if (vkCreateSemaphore(mDevice, &semaphoreCI, nullptr, &mImageAvailableSem) != VK_SUCCESS ||
        vkCreateSemaphore(mDevice, &semaphoreCI, nullptr, &mRenderFinishedSem) != VK_SUCCESS)
    {
        throw std::runtime_error("Unable to create semaphores!");
    }
}

void TutorialApp::cleanupSwapChain()
{
    // TODO - Create wrappers with automatic destruction.

    for (auto &fb : mSwapChainFramebuffers)
    {
        vkDestroyFramebuffer(mDevice, fb, nullptr);
    }

    vkDestroyImageView(mDevice, mMSAADepthImageView, nullptr);
    vkFreeMemory(mDevice, mMSAADepthImageMemory, nullptr);
    vkDestroyImage(mDevice, mMSAADepthImage, nullptr);

    vkDestroyImageView(mDevice, mDepthImageView, nullptr);
    vkFreeMemory(mDevice, mDepthImageMemory, nullptr);
    vkDestroyImage(mDevice, mDepthImage, nullptr);

    vkFreeCommandBuffers(mDevice, mCommandPool,
                         static_cast<uint32_t>(mCommandBuffers.size()),
                         mCommandBuffers.data());

    vkDestroyPipeline(mDevice, mPipeline, nullptr);
    vkDestroyPipelineLayout(mDevice, mPipelineLayout, nullptr);

    vkDestroyRenderPass(mDevice, mRenderPass, nullptr);

    vkDestroyImageView(mDevice, mMSAAImageView, nullptr);
    vkFreeMemory(mDevice, mMSAAImageMemory, nullptr);
    vkDestroyImage(mDevice, mMSAAImage, nullptr);

    for (auto &imageView : mSwapChainViews)
    {
        vkDestroyImageView(mDevice, imageView, nullptr);
    }
    //vkDestroySwapchainKHR(mDevice, mSwapChain, nullptr);
}

void TutorialApp::recreateSwapChain()
{
    // TODO - Use oldSwapChain field in VkSwapchainCreateInfo, without waiting for idle.
    vkDeviceWaitIdle(mDevice);

    cleanupSwapChain();

    createSwapChain();
    createSwapChainViews();
    createMSAAImageView();
    createRenderPass();
    createGraphicsPipeline();
    createDepthImageView();
    createFramebuffers();
    createCommandBuffers();
}

void TutorialApp::updateState()
{
    float time = glfwGetTime();
    mUniforms.model = glm::rotate(
            glm::mat4(1.0f),
            time * glm::radians(90.0f),
            glm::vec3(0.0f, 0.0f, 1.0f));
    mUniforms.view = glm::lookAt(
            glm::vec3(0.0f, 0.0f, 3.0f),
            glm::vec3(0.0f, 0.0f, 0.0f),
            glm::vec3(0.0f, 1.0f, 0.0f));
    mUniforms.proj = glm::perspective(
            glm::radians(60.0f),
            static_cast<float>(mSwapChainExtent.width) / mSwapChainExtent.height,
            0.1f, 100.0f);
    //mUniforms.proj[0][0] *= -1.0f;

    updateUBO();
}

void TutorialApp::updateUBO()
{
    void *data{nullptr};
    vkMapMemory(mDevice, mUniformMemory, 0, sizeof(mUniforms), 0, &data);
    {
        memcpy(data, &mUniforms, sizeof(mUniforms));
    }
    vkUnmapMemory(mDevice, mUniformMemory);
}

void TutorialApp::submitDrawCommands()
{
    // Get index of the next image in the swap chain.
    uint32_t imageIndex{0};
    auto result = vkAcquireNextImageKHR(mDevice,
                                            mSwapChain,
                                            // Disable timeout.
                                            std::numeric_limits<uint64_t>::max(),
                                            mImageAvailableSem,
                                            VK_NULL_HANDLE,
                                            &imageIndex);

    if (result == VK_ERROR_OUT_OF_DATE_KHR)
    { // Unusable, we need to recreate swap chain.
        recreateSwapChain();
        return;
    }
    else if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR)
    { // Function call failed.
        throw std::runtime_error("Unable to acquire next image!");
    }

    // Setup the draw command.
    VkSubmitInfo drawCommandSI{};
    drawCommandSI.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    // Wait for the image to be available.
    auto waitSemaphores = {mImageAvailableSem};
    std::initializer_list<VkPipelineStageFlags> waitStages = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
    drawCommandSI.waitSemaphoreCount = static_cast<uint32_t>(waitSemaphores.size());
    drawCommandSI.pWaitSemaphores = waitSemaphores.begin();
    drawCommandSI.pWaitDstStageMask = waitStages.begin();
    // Select the corresponding command buffer.
    drawCommandSI.commandBufferCount = 1;
    drawCommandSI.pCommandBuffers = &mCommandBuffers[imageIndex];
    // Signalize completion of rendering.
    auto signalSemaphores = {mRenderFinishedSem};
    drawCommandSI.signalSemaphoreCount = static_cast<uint32_t>(signalSemaphores.size());
    drawCommandSI.pSignalSemaphores = signalSemaphores.begin();

    // Submit the draw command.
    if (vkQueueSubmit(mGraphicsQueue, 1, &drawCommandSI, VK_NULL_HANDLE) != VK_SUCCESS)
    {
        throw std::runtime_error("Unable to sumbit draw command!");
    }

    // Setup presentation for the rendered image.
    VkPresentInfoKHR presentInfo{};
    presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
    // Wait for the image to be ready.
    presentInfo.waitSemaphoreCount = static_cast<uint32_t>(signalSemaphores.size());
    presentInfo.pWaitSemaphores = signalSemaphores.begin();
    // Submit into the main swap chain.
    auto swapChains = {mSwapChain};
    presentInfo.swapchainCount = static_cast<uint32_t>(swapChains.size());
    presentInfo.pSwapchains = swapChains.begin();
    // Present the current image.
    presentInfo.pImageIndices = &imageIndex;
    presentInfo.pResults = nullptr;

    result = vkQueuePresentKHR(mGraphicsQueue, &presentInfo);

    if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR)
    { // We should recreate the swap chain.
        recreateSwapChain();
        return;
    }
    else if (result != VK_SUCCESS)
    { // Function call failed.
        throw std::runtime_error("Unable to present image!");
    }
}

bool TutorialApp::checkValidationLayers()
{
    // Get number of layers.
    uint32_t layerCount{0};
    vkEnumerateInstanceLayerProperties(&layerCount, nullptr);

    // Get the actual layers.
    std::vector<VkLayerProperties> availableLayers(layerCount);
    vkEnumerateInstanceLayerProperties(&layerCount, availableLayers.data());

    // Check that all of the validation layers are available.
    uint32_t supportedValidLayers{0};
    for (const auto validationLayer : VALIDATION_LAYERS)
    {
        for (const auto &layer : availableLayers)
        {
            if (std::strcmp(validationLayer, layer.layerName) == 0)
            {
                supportedValidLayers++;
                break;
            }
        }
    }

    // Check, if the number of supported layers is the same as the number of requested layers.
    return supportedValidLayers == VALIDATION_LAYERS.size();
}

bool TutorialApp::isDeviceSuitable(VkPhysicalDevice device)
{
    auto indices = findQueueFamilies(device);
    bool extensionSupported{checkDeviceExtensionSupport(device)};

    bool swapChainSupported{false};
    if (extensionSupported)
    {
        auto details = querySwapChainDetails(device);
        swapChainSupported = !details.formats.empty() && !details.presentModes.empty();
    }

    VkPhysicalDeviceFeatures supportedFeatures{};
    vkGetPhysicalDeviceFeatures(device, &supportedFeatures);

    return indices.isComplete() &&
           extensionSupported &&
           swapChainSupported &&
           supportedFeatures.samplerAnisotropy;
}

bool TutorialApp::checkDeviceExtensionSupport(VkPhysicalDevice device)
{
    // Get the number of extensions.
    uint32_t extensionCount{0};
    vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, nullptr);

    // Get the actual extensions.
    std::vector<VkExtensionProperties> extensions(extensionCount);
    vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, extensions.data());

    // Make sure all of the required extensions are supported.
    for (const auto &reqExtension : DEVICE_EXTENSIONS)
    {
        bool supported{false};

        for (const auto &extension : extensions)
        {
            if (std::strcmp(reqExtension, extension.extensionName) == 0)
            {
                supported = true;
            }
        }

        if (!supported)
        {
            return false;
        }
    }

    return true;
}

std::vector<const char*> TutorialApp::getRequiredExtensions()
{
    std::vector<const char*> result;

    // Get the extensions required by GLFW.
    uint32_t glfwExtensionCount{0};
    const char **glfwExtensions{nullptr};
    glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

    // Copy GLFW extensions to the list.
    std::copy(glfwExtensions, glfwExtensions + glfwExtensionCount, std::back_inserter(result));

    if (VALIDATION_LAYERS_ENABLE)
    {
        // Add the validation layer extension.
        result.emplace_back(VK_EXT_DEBUG_REPORT_EXTENSION_NAME);
    }

    return result;
}

TutorialApp::QueueFamilyIndices TutorialApp::findQueueFamilies(VkPhysicalDevice device)
{
    QueueFamilyIndices indices;

    // Get the number of queue families.
    uint32_t familiesCount{0};
    vkGetPhysicalDeviceQueueFamilyProperties(device, &familiesCount, nullptr);
    if (familiesCount == 0)
    {
        return indices;
    }

    // Get the actual families.
    std::vector<VkQueueFamilyProperties> families(familiesCount);
    vkGetPhysicalDeviceQueueFamilyProperties(device, &familiesCount, families.data());

    // Check all the required families and remember their indices.
    for (uint32_t iii = 0; iii < families.size(); ++iii)
    {
        const auto &family = families[iii];
        if (family.queueCount > 0 && (family.queueFlags & VK_QUEUE_GRAPHICS_BIT))
        {
            indices.graphicsFamily = iii;
        }
        VkBool32 presentSupported{VK_FALSE};
        vkGetPhysicalDeviceSurfaceSupportKHR(device, iii, mSurface, &presentSupported);
        if (family.queueCount > 0 && presentSupported)
        {
            indices.presentFamily = iii;
        }
    }

    return indices;
}

TutorialApp::SwapChainSupportDetails TutorialApp::querySwapChainDetails(VkPhysicalDevice device)
{
    SwapChainSupportDetails details{};

    // Get surface capabilities.
    vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, mSurface, &details.capabilities);

    // Get number of surface formats.
    uint32_t formatCount{0};
    vkGetPhysicalDeviceSurfaceFormatsKHR(device, mSurface, &formatCount, nullptr);

    if (formatCount != 0)
    {
        // Get the actual formats.
        details.formats.resize(formatCount);
        vkGetPhysicalDeviceSurfaceFormatsKHR(device, mSurface, &formatCount, details.formats.data());
    }

    // Get the number of presentation modes.
    uint32_t presentModeCount{0};
    vkGetPhysicalDeviceSurfacePresentModesKHR(device, mSurface, &presentModeCount, nullptr);

    if (formatCount != 0)
    {
        // Get the actual presentation modes.
        details.presentModes.resize(presentModeCount);
        vkGetPhysicalDeviceSurfacePresentModesKHR(device, mSurface, &presentModeCount, details.presentModes.data());
    }

    return details;
}

VkSurfaceFormatKHR TutorialApp::chooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats)
{
    if (availableFormats.size() == 1 && availableFormats[0].format == VK_FORMAT_UNDEFINED)
    { // We are free to use any format we want.
        return { VK_FORMAT_B8G8R8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR };
    }

    for (const auto &format : availableFormats)
    {
        if (format.format == VK_FORMAT_B8G8R8A8_UNORM &&
            format.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR)
        { // We found our preferred format.
            return format;
        }
    }

    // Preferred format not found, return the first one.
    return availableFormats[0];
}

VkPresentModeKHR TutorialApp::chooseSwapPresentMode(const std::vector<VkPresentModeKHR> &presentModes)
{
    // TODO - Add constant for enabling v-sync.
    // VK_PRESENT_MODE_FIFO_KGR should be supported, but some drivers don't support it.
    VkPresentModeKHR fallbackMode{VK_PRESENT_MODE_FIFO_KHR};

    for (const auto &mode : presentModes)
    {
        if (mode == VK_PRESENT_MODE_MAILBOX_KHR)
        { // Our preferred one.
            return mode;
        }
        else if (mode == VK_PRESENT_MODE_IMMEDIATE_KHR)
        {
            fallbackMode = VK_PRESENT_MODE_IMMEDIATE_KHR;
        }
    }

    return fallbackMode;
}

VkExtent2D TutorialApp::chooseSwapExtent(const VkSurfaceCapabilitiesKHR &capabilities)
{
    if (capabilities.currentExtent.width != std::numeric_limits<uint32_t>::max())
    { // We can use the specified resolution.
        return capabilities.currentExtent;
    }
    else
    { // Choose resolution supported by window manager.
        int width;
        int height;
        glfwGetWindowSize(mWindow, &width, &height);

        VkExtent2D preferredExtent{static_cast<uint32_t>(width), static_cast<uint32_t>(height)};

        // Select resolution acceptable by window manager.
        preferredExtent.width = std::max(capabilities.minImageExtent.width,
                                         std::min(capabilities.maxImageExtent.width,
                                                  preferredExtent.width));
        preferredExtent.height = std::max(capabilities.minImageExtent.height,
                                         std::min(capabilities.maxImageExtent.height,
                                                  preferredExtent.height));

        return preferredExtent;
    }
}
VkShaderModule TutorialApp::createShaderModule(const std::vector<char> &shaderSpirV)
{
    VkShaderModuleCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    createInfo.codeSize = shaderSpirV.size();
    createInfo.pCode = reinterpret_cast<const uint32_t*>(shaderSpirV.data());

    VkShaderModule shaderModule;
    if (vkCreateShaderModule(mDevice, &createInfo, nullptr, &shaderModule) != VK_SUCCESS)
    {
        throw std::runtime_error("Unable to create shader module!");
    }

    return shaderModule;
}

uint32_t TutorialApp::findMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties)
{
    VkPhysicalDeviceMemoryProperties memProps{};
    vkGetPhysicalDeviceMemoryProperties(mPhysicalDevice, &memProps);

    for (uint32_t iii = 0; iii < memProps.memoryTypeCount; ++iii)
    {
        // Check the type of the memory and supported properties are all available.
        if ((typeFilter & (1 << iii)) &&
            (memProps.memoryTypes[iii].propertyFlags & properties) == properties)
        {
            return iii;
        }
    }

    throw std::runtime_error("Unable to find memory with required type/properties.");
}

void TutorialApp::createBuffer(VkDeviceSize size, VkBufferUsageFlags usage,
                               VkMemoryPropertyFlags properties, VkBuffer &buffer,
                               VkDeviceMemory &memory)
{
    // Create the buffer.
    VkBufferCreateInfo bufferCI{};
    bufferCI.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    // Specify size of the buffer.
    bufferCI.size = size;
    // Presumed usage of the buffer..
    bufferCI.usage = usage;
    // Buffer will be used only from a single queue.
    bufferCI.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

    if (vkCreateBuffer(mDevice, &bufferCI, nullptr, &buffer) != VK_SUCCESS)
    {
        throw std::runtime_error("Unable to create buffer!");
    }

    // Check how much memory will the buffer need.
    VkMemoryRequirements memReq{};
    vkGetBufferMemoryRequirements(mDevice, buffer, &memReq);

    // Allocate memory for the buffer.
    VkMemoryAllocateInfo memAI{};
    memAI.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    // Pass the required size.
    memAI.allocationSize = memReq.size;
    // Find suitable memory.
    memAI.memoryTypeIndex = findMemoryType(memReq.memoryTypeBits, properties);

    if (vkAllocateMemory(mDevice, &memAI, nullptr, &memory) != VK_SUCCESS)
    {
        throw std::runtime_error("Unable to allocate vertex memory!");
    }

    // Connect the allocated memory with the buffer.
    vkBindBufferMemory(mDevice, buffer, memory, 0);
}

void TutorialApp::copyBuffer(VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size)
{
    auto cmdBuffer = beginTransientCommands();
    {
        // Actual copy command.
        VkBufferCopy bufferCopy{};
        bufferCopy.srcOffset = 0;
        bufferCopy.dstOffset = 0;
        bufferCopy.size = size;
        vkCmdCopyBuffer(cmdBuffer, srcBuffer, dstBuffer, 1, &bufferCopy);
    }
    endTransientCommands(cmdBuffer);
}

void TutorialApp::copyBufferToImage(VkBuffer buffer, VkImage image,
                                    uint32_t width, uint32_t height)
{
    auto cmdBuffer = beginTransientCommands();
    {
        VkBufferImageCopy bufferImageCopy{};
        // We are copying into the buffer, no need to specify.
        bufferImageCopy.bufferOffset = 0;
        bufferImageCopy.bufferRowLength = 0;
        bufferImageCopy.bufferImageHeight = 0;
        // No mip-mapping or layers.
        bufferImageCopy.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        bufferImageCopy.imageSubresource.mipLevel = 0;
        bufferImageCopy.imageSubresource.baseArrayLayer = 0;
        bufferImageCopy.imageSubresource.layerCount = 1;
        // Copy whole image.
        bufferImageCopy.imageOffset = { 0, 0, 0};
        bufferImageCopy.imageExtent.width = width;
        bufferImageCopy.imageExtent.height = height;
        bufferImageCopy.imageExtent.depth = 1;

        // Specify the copy command.
        vkCmdCopyBufferToImage(
                cmdBuffer,
                buffer, image,
                VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                1, &bufferImageCopy);
    }
    endTransientCommands(cmdBuffer);
}

void TutorialApp::createImage(uint32_t width, uint32_t height, VkFormat format,
                 VkSampleCountFlagBits samples, VkImageTiling tiling,
                 VkImageUsageFlags usageFlags, VkMemoryPropertyFlags memFlags,
                 VkImage &image, VkDeviceMemory &memory)
{
    // Create the image.
    VkImageCreateInfo imageCI{};
    imageCI.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    imageCI.flags = 0;
    imageCI.imageType = VK_IMAGE_TYPE_2D;
    imageCI.format = format;
    imageCI.extent.width = width;
    imageCI.extent.height = height;
    imageCI.extent.depth = 1;
    imageCI.mipLevels = 1;
    imageCI.arrayLayers = 1;
    imageCI.samples = samples;
    imageCI.tiling = tiling;
    imageCI.usage = usageFlags;
    imageCI.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    imageCI.queueFamilyIndexCount = 0;
    imageCI.pQueueFamilyIndices = nullptr;
    imageCI.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    if (vkCreateImage(mDevice, &imageCI, nullptr, &image) != VK_SUCCESS)
    {
        throw std::runtime_error("Unable to create image!");
    }

    // Allocate memory for the image.
    VkMemoryRequirements memoryReqs{};
    vkGetImageMemoryRequirements(mDevice, image, &memoryReqs);
    VkMemoryAllocateInfo memoryAI{};
    memoryAI.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    memoryAI.allocationSize = memoryReqs.size;
    memoryAI.memoryTypeIndex = findMemoryType(memoryReqs.memoryTypeBits, memFlags);
    if (vkAllocateMemory(mDevice, &memoryAI, nullptr, &memory) != VK_SUCCESS)
    {
        throw std::runtime_error("Unable to allocate memory for image!");
    }

    // Bind the allocated memory to the MSAA image.
    vkBindImageMemory(mDevice, image, memory, 0);
}

void TutorialApp::createImageView(const VkImage &image, VkFormat format,
                                  VkImageAspectFlags aspectFlags, VkImageView &imageView)
{
    // Create the image view.
    VkImageViewCreateInfo imageViewCI{};
    imageViewCI.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    imageViewCI.image = image;
    imageViewCI.viewType = VK_IMAGE_VIEW_TYPE_2D;
    imageViewCI.format = format;
    imageViewCI.components.r = VK_COMPONENT_SWIZZLE_R;
    imageViewCI.components.g = VK_COMPONENT_SWIZZLE_G;
    imageViewCI.components.b = VK_COMPONENT_SWIZZLE_B;
    imageViewCI.components.a = VK_COMPONENT_SWIZZLE_A;
    imageViewCI.subresourceRange.aspectMask = aspectFlags;
    imageViewCI.subresourceRange.baseMipLevel = 0;
    imageViewCI.subresourceRange.levelCount = 1;
    imageViewCI.subresourceRange.baseArrayLayer = 0;
    imageViewCI.subresourceRange.layerCount = 1;
    if (vkCreateImageView(mDevice, &imageViewCI, nullptr, &imageView) != VK_SUCCESS)
    {
        throw std::runtime_error("Unable to create image view!");
    }
}

VkFormat TutorialApp::findSupportedFormat(const std::initializer_list<VkFormat>& candidates,
                                          VkImageTiling tiling, VkFormatFeatureFlags features)
{
    for (auto &format : candidates)
    {
        VkFormatProperties properties;
        vkGetPhysicalDeviceFormatProperties(mPhysicalDevice, format, &properties);
        if (tiling == VK_IMAGE_TILING_OPTIMAL &&
           (properties.optimalTilingFeatures & features) == features)
        {
            return format;
        }
        else if (tiling == VK_IMAGE_TILING_LINEAR &&
                (properties.linearTilingFeatures & features) == features)
        {
            return format;
        }
    }

    throw std::runtime_error("Unable to find supported format!");
}

VkFormat TutorialApp::findDepthFormat()
{
    return findSupportedFormat(
            {VK_FORMAT_D32_SFLOAT_S8_UINT, VK_FORMAT_D24_UNORM_S8_UINT, VK_FORMAT_D32_SFLOAT},
            VK_IMAGE_TILING_OPTIMAL,
            VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT);
}

bool TutorialApp::hasStencilComponent(VkFormat format)
{
    return format == VK_FORMAT_D32_SFLOAT_S8_UINT || format == VK_FORMAT_D24_UNORM_S8_UINT;
}

VkCommandBuffer TutorialApp::beginTransientCommands()
{
    // TODO - Create a permanent command buffer for temporary commands. (VK_COMMAND_POOL_CREATE_TRANSIENT_BIT)

    // Create a temporary command buffer for the commands.
    VkCommandBufferAllocateInfo commandBufferCI{};
    commandBufferCI.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    commandBufferCI.commandPool = mCommandPool;
    commandBufferCI.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    commandBufferCI.commandBufferCount = 1;

    VkCommandBuffer tempCommandBuffer;
    if (vkAllocateCommandBuffers(mDevice, &commandBufferCI, &tempCommandBuffer) != VK_SUCCESS)
    {
        throw std::runtime_error("Unable to allocate copy command buffer!");
    }

    // Start recording commands.
    VkCommandBufferBeginInfo beginInfo{};
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
    vkBeginCommandBuffer(tempCommandBuffer, &beginInfo);

    return tempCommandBuffer;
}

void TutorialApp::endTransientCommands(VkCommandBuffer &commandBuffer)
{
    // Finalize the command buffer.
    vkEndCommandBuffer(commandBuffer);

    // Submit recorded commands for processing.
    VkSubmitInfo submitInfo{};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.waitSemaphoreCount = 0;
    submitInfo.pWaitSemaphores = nullptr;
    submitInfo.pWaitDstStageMask = 0;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &commandBuffer;
    submitInfo.signalSemaphoreCount = 0;
    submitInfo.pSignalSemaphores = nullptr;

    // Submit the copy command and wait for it to finish.
    vkQueueSubmit(mGraphicsQueue, 1, &submitInfo, VK_NULL_HANDLE);
    vkQueueWaitIdle(mGraphicsQueue);

    // Free the temporary command buffer.
    vkFreeCommandBuffers(mDevice, mCommandPool, 1, &commandBuffer);
}

VkImageAspectFlags TutorialApp::transitionImageAspects(VkFormat format, VkImageLayout newLayout)
{
    // TODO - Create an Image class.
    if (newLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL)
    {
        if (hasStencilComponent(format))
        {
            return VK_IMAGE_ASPECT_DEPTH_BIT | VK_IMAGE_ASPECT_STENCIL_BIT;
        }
        else
        {
            return VK_IMAGE_ASPECT_DEPTH_BIT;
        }
    }
    else
    {
        return VK_IMAGE_ASPECT_COLOR_BIT;
    }
}

void TutorialApp::transitionImageLayout(VkImage image, VkFormat format,
                                        VkImageLayout oldLayout, VkImageLayout newLayout)
{
    // TODO - Create an Image class.
    auto cmdBuffer = beginTransientCommands();
    {
        VkImageMemoryBarrier barrier{};
        barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
        // Specify the requested layout transition.
        barrier.oldLayout = oldLayout;
        barrier.newLayout = newLayout;
        barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        barrier.image = image;
        barrier.subresourceRange.aspectMask = transitionImageAspects(format, newLayout);
        barrier.subresourceRange.baseMipLevel = 0;
        barrier.subresourceRange.levelCount = 1;
        barrier.subresourceRange.baseArrayLayer = 0;
        barrier.subresourceRange.layerCount = 1;

        VkPipelineStageFlags srcStage;
        VkPipelineStageFlags dstStage;

        if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED &&
            newLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL)
        {
            // Preparing image for receiving data.
            // Can start at any time.
            barrier.srcAccessMask = 0;
            srcStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
            // Operations which require write access should wait.
            barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
            dstStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
        }
        else if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED &&
                 newLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL)
        {
            // Prepare depth/stencil buffer for usage in the pipeline.
            // Can start at any time.
            barrier.srcAccessMask = 0;
            srcStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
            // Should be completed before access for early tests.
            barrier.dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT |
                                    VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
            dstStage = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
        }
        else if (oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL &&
                 newLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
        {
            // Prepare loaded image for sampling.
            // Wait for the copy operation to complete.
            barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
            srcStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
            // Sampled from fragment shader.
            barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
            dstStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
        }
        else
        {
            throw std::runtime_error("Unsupported layout transition!");
        }

        // Add command to perform the transition.
        vkCmdPipelineBarrier(
                cmdBuffer,
                srcStage, dstStage,
                0,
                0, nullptr,
                0, nullptr,
                1, &barrier
        );
    }
    endTransientCommands(cmdBuffer);
}
